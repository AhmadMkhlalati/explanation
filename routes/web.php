<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\HomeController::class, 'home'])->name('/');
Route::get('/blog/{slug}', [\App\Http\Controllers\HomeController::class, 'BlogBySlug'])->name('BlogBySlug');
Route::get('/category/{slug}', [\App\Http\Controllers\HomeController::class, 'CategoryBySlug'])->name('CategoryBySlug');
Route::post('/subscribe', [\App\Http\Controllers\SubscribeController::class, 'subscribe'])->name('subscribe');
Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');
Route::get('/about', function () {
    return view('pages.about');
})->name('about');
Route::get('/search', [BlogController::class, 'search'])->name('search');
Route::post('/message/create', [\App\Http\Controllers\MessageController::class, 'create'])->name('message.create');
Route::get('policy', function () {
    return view('pages.policy');
})->name('policy');
Route::get('term', function () {
    return view('pages.term');
})->name('term');


Route::get('/categories', function () {
    return view('pages.categories');
})->name('categories');
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');

    // Route::resource('users', 'UserController');
    Route::resource('blogs', BlogController::class);
    // Route::resource('categories', 'CategoryController');
    // Route::resource('marks', 'MarkController');
});

require __DIR__ . '/auth.php';
