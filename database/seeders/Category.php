<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::query()->insert([
            ['name'=>'برمجة مواقع'],
            ['name'=>'برمجة تطبيقات'],
            ['name'=>' تكنولوجيا'],
        ]);
    }
}
