<?php

namespace Database\Seeders;

use App\Models\Mark;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Marks extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mark::query()->insert([[
            'name'=>'مميز',
        ],
            [
                'name'=>'جديد',
            ],
            [
            'name'=>'رائج',
        ]]);
    }
}
