<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // $faker = \Faker\Factory::create();

        $data = [
            'name'     => 'Ahmad Mk',
            'email'    => 'Ahmad@gmail.com',
            'password' => bcrypt('123456789'),
            'role'     => 10,
            'bio'      => '$faker->realText()',
        ];

        User::query()->insert($data);
    }
}
