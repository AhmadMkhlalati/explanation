
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Blog') }}
        </h2>
    </x-slot>

    <div class="py-12 container mx-auto">
        <a href="{{route('blogs.create')}}" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">New Blog</a>

        <div class="relative overflow-x-auto shadow-md sm:rounded-lg mt-5">
           

            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400 rounded ">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="px-6 py-5">
                            Slug
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Title
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Visits
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Mark id
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Category id
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Created
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Updated
                        </th>
                        <th scope="col" class="px-6 py-5">
                            Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $blog)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$blog->slug}}
                        </th>
                        <td class="px-6 py-4">
                            {{$blog->title}}
                        </td>
                        <td class="px-6 py-4">
                            {{$blog->visits}}
                        </td>
                        <td class="px-6 py-4">
                            {{$blog->mark_id}}                        
                        </td>
                        <td class="px-6 py-4">
                            {{$blog->category_id}}                        
                        </td>
                        <td class="px-6 py-4">
                            {{$blog->created_at}}
                        </td>
                        <td class="px-6 py-4">
                            {{$blog->updated_at}}
                        </td>
                        <td class="px-6 py-4">
                            <a href="#" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a>
                        </td>
                    </tr> 
                    @endforeach
                   
                  
                </tbody>

            </table>
        </div>
        {{$blogs->links()}}

    </div>
</x-app-layout>