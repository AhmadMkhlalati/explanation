@extends('layouts.master')

@section('content')
<div class=" mx-auto bg-white text-center ">
    <div class="container">
    <div class="row  ">
        <img class="max-w-2xl mx-auto" src="{{asset('/images/not_found.png')}}"/>
        <h1 class="mb-4 text-4xl tracking-tight  text-gray-900 dark:text-white mx-auto">Page Not Found</h1>
        <div class="text-center py-4">
                            <a href="{{route('/')}}"
                                class="py-3 mt-8 px-5 text-sm font-medium text-center text-white rounded-lg border cursor-pointer bg-blue-700 border-blue-600 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
Home                            </a>
        </div>
    </div>
    </div>
</div>
@endsection
