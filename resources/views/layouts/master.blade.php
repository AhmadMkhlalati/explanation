<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/png" href="{{asset('logo.png')}}"/>
        @if(isset($title))
        @include('layouts._partials.meta')
        @else
                    <title>{{ config('app.name', 'Laravel') }}</title>

        @endif

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="{{asset('build/assets/app-82fa8b88.css')}}">
        <script  src="{{asset('build/assets/app-7b452953.js')}}"></script>

        <!-- Scripts -->
{{--         @vite(['resources/css/app.css', 'resources/js/app.js'])--}}
        <script src="lottie.min.js" ></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">

            <!-- Page Heading -->
            @include('layouts._partials.header')
            <!-- Page Content -->
            <main>
                @yield('content')
            </main>
            @include('layouts._partials.footer')

        </div>
    </body>
</html>
