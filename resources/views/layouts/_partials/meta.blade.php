<meta charset="UTF-8">
<title><?= $title ?> - Explanations</title>
{{--<base href="<?= getMainData("url") ?>"/>--}}
{{--<link rel="icon" type="image/png" href="{{asset('assets/web/images/final-logo.png')}}"/>--}}
{{--<meta name="viewport" content="width=device-width, initial-scale=1">--}}

<meta name="robots" content="index,follow">
<meta property="og:type" content="<?= $type ?>"/>
<meta property="og:title" content="<?= $title ?>"/>
<meta property="og:description" content="<?= $description ?>"/>
<meta property="og:image" content="<?= $image ?>"/>
<meta property="og:url" content="{{url()->current()}}"/>
<meta property="og:site_name" content="<?= $title ?>"/>
{{--<meta property="fb:app_id" content="1148741481823499"/>--}}
<meta property="article:author" content="{{url()->current()}}"/>
<meta property="article:publisher" content="{{url()->current()}}"/>

<meta name="twitter:card" content="<?= $type ?>">
<meta name="twitter:domain" content="<?= route('/') ?>">
<meta name="twitter:site" content="<?= $title ?>">
<meta name="twitter:creator" content="<?= $title ?>">
<meta name="twitter:image" content="<?= $image ?>">
<meta name="twitter:image:src" content="<?= $image ?>">
<meta name="twitter:description" content="<?= $description ?>">
<meta name="twitter:title" content="<?= $title ?>">
<meta name="twitter:url" content="{{url()->current()}}">

<meta name="title" content="<?= $title ?>">
<meta name="description" content="<?= $description ?>">
<meta name="keywords" content="<?= $keywords ?>"/>
<meta name="@context" content="https://schema.org/">
<meta name="@type" content="<?= $type ?>">
<meta name="author" content="ExplanToMe">
<meta name="rating" content="General">
