<link rel="stylesheet" href="{{ asset('css/quill.snow.css') }}">

<script src="{{ asset('js/quill.js') }}"></script>
<div class="my-6">
<p class="text-sm my-2">introduction</p>
<div id="introduction-editor" class="mb-5">
    @if (isset($item))
        {!! $item->introduction !!}
    @else
        <p>Hello Worlddd!</p>
        <p>Some initial <strong>bold</strong> text</p>
        <p><br></p>
    @endif
</div>
<input type='hidden' name='introduction' id='introduction'>
</div>
<div class="my-6">
<p class="text-sm my-2">body</p>

<div id="body-editor" class="mb-5">
    @if (isset($item))
        {!! $item->body !!}
    @else
        <p>Hello Worlllld!</p>
        <p>Some initial <strong>bold</strong> text</p>
        <p><br></p>
    @endif
</div>

<input type='hidden' name='body' id='body'>
</div>
<div class="my-6">
<p class="text-sm my-2">conclusion</p>

<div id="conclusion-editor" class="mb-5 ">
    @if (isset($item))
        {!! $item->conclusion !!}
    @else
        <p>Hello Worrrrld!</p>
        <p>Some initial <strong>bold</strong> text</p>
        <p><br></p>
    @endif
</div>
<input type='hidden' name='conclusion' id='conclusion'>
</div>
<!-- Initialize Quill editor -->
<script>
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'], // toggled buttons
        ['blockquote', 'code-block'],
        ['image', 'link'],
        [{
            'header': 1
        }, {
            'header': 2
        }], // custom button values
        [{
            'list': 'ordered'
        }, {
            'list': 'bullet'
        }],
        [{
            'script': 'sub'
        }, {
            'script': 'super'
        }], // superscript/subscript
        [{
            'indent': '-1'
        }, {
            'indent': '+1'
        }], // outdent/indent
        [{
            'direction': 'rtl'
        }], // text direction

        [{
            'size': ['small', false, 'large', 'huge']
        }], // custom dropdown
        [{
            'header': [1, 2, 3, 4, 5, 6, false]
        }],

        [{
            'color': []
        }, {
            'background': []
        }], // dropdown with defaults from theme
        [{
            'font': []
        }],
        [{
            'align': []
        }],

        ['clean'], // remove formatting button

    ];

    var quill_body = new Quill('#body-editor', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });
    var quill_introduction = new Quill('#introduction-editor', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });
    var quill_conclusion = new Quill('#conclusion-editor', {
        modules: {
            toolbar: toolbarOptions
        },
        theme: 'snow'
    });
    var inputElement_body = document.getElementById('body')
    var inputElement_introduction = document.getElementById('introduction')
    var inputElement_conclusion = document.getElementById('conclusion')
    // console.log(inputElement_conclusion)
    quill_body.on('text-change', async function(delta, oldDelta, source) {

        const imgs = Array.from(
            quill_body.container.querySelectorAll('img[src^="data:"]:not(.loading)')
        );
        for (const img of imgs) {
            img.classList.add("loading");
            img.setAttribute("src", await uploadBase64Img(img.getAttribute("src")));
            img.classList.remove("loading");
        }
        inputElement_body.value = quill_body.container.innerHTML

        // console.log(inputElement_body.container.innerHTML)

    });
    quill_introduction.on('text-change', async function(delta, oldDelta, source) {
        const imgs = Array.from(
            quill_introduction.container.querySelectorAll('img[src^="data:"]:not(.loading)')
        );
        for (const img of imgs) {
            img.classList.add("loading");
            img.setAttribute("src", await uploadBase64Img(img.getAttribute("src")));
            img.classList.remove("loading");
        }
        inputElement_introduction.value = quill_introduction.container.innerHTML
        // console.log(inputElement_introduction.container.innerHTML)

    });
    quill_conclusion.on('text-change', async function(delta, oldDelta, source) {
        const imgs = Array.from(
            quill_conclusion.container.querySelectorAll('img[src^="data:"]:not(.loading)')
        );
        for (const img of imgs) {
            img.classList.add("loading");
            img.setAttribute("src", await uploadBase64Img(img.getAttribute("src")));
            img.classList.remove("loading");
        }
        inputElement_conclusion.value = quill_conclusion.container.innerHTML
        // console.log(inputElement_conclusion.container.innerHTML)

    });

    async function uploadBase64Img(base64Str) {
    if (typeof base64Str !== 'string' || base64Str.length < 100) {
        return base64Str;
    }
    const url = await b64ToUrl(base64Str);
    return url;
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
   contentType = contentType || '';
   sliceSize = sliceSize || 512;

   var byteCharacters = atob(b64Data);
   var byteArrays = [];

   for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
       var slice = byteCharacters.slice(offset, offset + sliceSize);

       var byteNumbers = new Array(slice.length);
       for (var i = 0; i < slice.length; i++) {
           byteNumbers[i] = slice.charCodeAt(i);
       }

       var byteArray = new Uint8Array(byteNumbers);

       byteArrays.push(byteArray);
   }

   var blob = new Blob(byteArrays, {type: contentType});
   return blob;
}
function b64ToUrl(base64) {
  return new Promise(resolve => {
    // Split the base64 string in data and contentType
    var block = base64.split(";");
    // Get the content type of the image
    var contentType = block[0].split(":")[1];
    // get the real base64 content of the file
    var realData = block[1].split(",")[1];
    // Convert it to a blob to upload
    var blob = b64toBlob(realData, contentType);
    console.log(blob);
    console.log(realData);
    console.log(block);
    // create form data
    const fd = new FormData();
    // replace "file_upload" with whatever form field you expect the file to be uploaded to
    fd.append('file_upload', blob);

    const xhr = new XMLHttpRequest();
    // replace "/upload" with whatever the path is to your upload handler
    xhr.open('POST', '{{route('upload')}}', true);
    // xhr.setRequestHeader('authorization', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMCIsImF1dGgiOnsiY29sbGFib3JhdGlvbiI6eyIqIjp7InJvbGUiOiJ3cml0ZXIifX0sImNrYm94Ijp7InJvbGUiOiJhZG1pbiJ9fSwidXNlciI6eyJpZCI6IjEwIiwibmFtZSI6IkFtZWxpYSBCcm9tcHRvbiIsImVtYWlsIjoidGFAcmlqYnVwc3VkLnNyIn0sImlzRGV2VG9rZW4iOnRydWUsInRpbWVzdGFtcCI6MTY3OTA5MDUzNjQ3MCwic2lnbmF0dXJlIjoiZDk1ZGI0YjRhZmZjZjE5ZjEwZDU1MjJmZTNiNTMzODc0ZTM1MjRjOGExNjY1Y2JlNzI1NDdhNjMwZGRlZjg5YiIsImV4cCI6MTY3OTA5NDEzNiwiYXVkIjoia1NXN1Y5TkhVWHVndmhvUWVGYWYiLCJqdGkiOiJJUWQzT2MtaVNycGdfOE9FQ1lxekpIOHZ6UzNsVjhabCIsImlhdCI6MTY3OTA5MDUzNn0.HNrBmQl6B7ScojyM407zFFY1lwnAz92evnhz36UyTwg');

    xhr.onload = () => {
      if (xhr.status === 200) {
        // my upload handler responds with JSON of { "path": "/images/static_images/blob2.png" }
        const url = JSON.parse(xhr.responseText).default;
        resolve(url);
      }
    };
    xhr.send(fd);
  });
}
</script>
