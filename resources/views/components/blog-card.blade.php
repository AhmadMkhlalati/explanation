

<article class=" ">
    <a href="{{route('BlogBySlug',$blog->slug)}}" class="relative ">
        {{-- <div class="absolute  z-10 top-2 ml-2">
            <div class="row">
                 <span class="inline-block text-xs font-medium   px-2.5 py-0.5 rounded " style="    background: #bb2d2d9c;
    color: #fff;">{{$blog['mark']->flag}}</span>
                <span class="inline-block text-xs font-medium  px-2.5 py-0.5 rounded " style="    background: #2563eb;
    color: #fff;">{{$blog['category']->name}}</span>
            </div>

        </div> --}}

        <img src="{{$blog->image}}" class="mb-5 rounded-lg" alt="Image 1">
    </a>
    <h2 class="mb-2 text-xl font-bold leading-tight text-gray-900">
        <a href="{{route('BlogBySlug',$blog->slug)}}">{{$blog->title}}</a>
    </h2>
    <p class="mb-4 font-light text-gray-500">{{$blog->meta_description}}</p>
    <a href="{{route('BlogBySlug',$blog->slug)}}"  class="inline-flex items-center font-medium underline underline-offset-4 text-blue-600 hover:no-underline">
      Read More             </a>
</article>

