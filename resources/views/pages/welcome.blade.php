@extends('layouts.master')

@section('content')
    <section class="bg-white dark:bg-gray-900 ">
        <div class="grid max-w-screen-xl px-4 mx-auto lg:gap-8 xl:gap-0 lg:py-16 lg:grid-cols-12">

            <div class="lg:mt-0 lg:col-span-5 lg:flex bg-white dark:bg-gray-900">
                {{-- <lottie :options="manOptions" v-on:animCreated="handleAnimation" /> --}}
                <div id="animation-hero"></div>

            </div>
            <div class="mr-auto place-self-center lg:col-span-7 sm:mt-16 ">
                <h1 class="max-w-2xl my-5 text-4xl tracking-tight leading-none md:text-5xl xl:text-6xl dark:text-white">
                    The Best way to searching and reading articles
                </h1>
                <div class="max-w-screen-sm my-4 justify-center md:mx-16 sm:mx-0">
                    <form method="get" action="{{route('search')}}">
                        @csrf
                        <label for="search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                        <div class="relative">
                            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                            </div>
                            <input type="search" id="search" name="query" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search" required>
                            <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>

    <!-- ====== Video Section Start -->
    <!-- <section class="py-8 px-4 mx-auto max-w-screen-xl ">
      <div class="mx-auto max-w-screen-md sm:text-center">
     <video class="w-full max-w-2xl h-auto rounded-lg border border-gray-200 dark:border-gray-700" controls>
      <source src="https://www.youtube.com/watch?v=7MBDb6jzhI4" type="video/mp4">
      Your browser does not support the video tag.
    </video>
    </div>
    </section> -->
    <!-- ====== Video Section End -->
    <section aria-label="Related articles" class="py-4 lg:py-24 bg-gray-50 dark:bg-gray-800">
        <div class="px-4 my-4 mx-auto max-w-screen-xl text-center">
            <h2 class="mb-4 text-3xl lg:text-4xl text-gray-900 dark:text-white">
                Recently added articles
            </h2>
            <p class="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                These are the latest added articles that you can view and share.
            </p>
            <div class="grid mt-4 gap-6 sm:grid-cols-2 lg:grid-cols-4">
                @foreach($data['new'] as $blog)
                @include('components.blog-card', ['blog'=>$blog])
                @endforeach
            </div>
        </div>
    </section>
    <section class="bg-white dark:bg-gray-900">
        <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
            <div class="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
                <h2 class="mb-4 text-3xl lg:text-4xl tracking-tight text-gray-900 dark:text-white">
                    Featured articles
                </h2>
                <p class="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                    These are the latest added articles that you can view and share.
                </p>
            </div>
            <div class="grid mt-4 gap-6 sm:grid-cols-2 lg:grid-cols-4">
                @foreach($data['feature'] as $blog)
                    @include('components.blog-card', ['blog'=>$blog])
                @endforeach

            </div>
            <div class="text-center">
{{--                <button type="submit"--}}
{{--                    class="py-3 mt-8 px-5 text-sm font-medium text-center text-white rounded-lg border cursor-pointer bg-blue-700 border-blue-600 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">--}}
{{--                    load more--}}
{{--                </button>--}}
            </div>
        </div>
    </section>
    <section class="bg-blue-600 dark:bg-blue-900">
        <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
            <div class="mx-auto max-w-screen-md sm:text-center">
                <h2 class="mb-4 text-3xl tracking-tight sm:text-4xl text-white">
                    Join us now and for free
                </h2>
                <p class="mx-auto mb-8 max-w-2xl font-light text-white md:mb-12 sm:text-xl">To receive all new directly, please enter your e-mail to stay on
                    Connect with everything that goes around the world of technology</p>
                <form action="{{route('subscribe')}}" method="post">
                    @csrf
                    <div class="items-center mx-auto mb-3 space-y-4 max-w-screen-sm sm:flex sm:space-y-0">

                        <div>
                            <button type="submit"
                                class="py-3 px-5 w-full  font-medium text-center text-white rounded-lg border cursor-pointer bg-blue-700 border-blue-600 sm:rounded-none sm:rounded-l-lg hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                                subscription
                            </button>
                        </div>
                        <div class="relative w-full">
                            <label for="email"
                                class="hidden mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Enter email here
                            </label>
                            <div class="flex absolute inset-y-0 left-0 items-center pl-3 pointer-events-none">
                                <svg class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor"
                                    viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                                </svg>
                            </div>
                            <input
                                class="block p-3 pl-10 w-full text-sm text-gray-900 bg-white rounded-lg border border-gray-300 sm:rounded-none sm:rounded-r-lg focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                placeholder="Enter email here"type="email" id="email" required="" name="email"/>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </section>

    <script>
        var animation = bodymovin.loadAnimation({

            container: document.getElementById('animation-hero'),

            path: '{{ asset('lottie/man-home.json') }}',

            renderer: 'svg',

            loop: true,

            autoplay: true,

            name: "Space Man",

        });
        // var animationbg = bodymovin.loadAnimation({

        //     container: document.getElementById('animation-bg'),

        //     path: '{{ asset('lottie/hero-bg.json') }}',

        //     renderer: 'svg',

        //     loop: true,

        //     autoplay: true,

        //     name: "backgroud Animation",

        // });
    </script>
@endsection
