
@extends('layouts.master')

@section('content')


    <section class="bg-white dark:bg-gray-900">
        <div class="flex justify-center py-5">
            <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white mx-auto">Welcome to Explanation Site</h2>
        </div>
    </section>
    <section class="bg-white dark:bg-gray-900">
    <div class="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
        <img class="w-full " src="{{asset('images/goals.png')}}" alt="goals image">
        <div class="mt-4 md:mt-0">
            <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Our Goal is </h2>
            <p class="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400">where we strive to provide informative and educational content on a wide range of topics. Our goal is to make complex ideas and concepts easy to understand for everyone.</p>

        </div>
    </div>
</section>

<section class="bg-white dark:bg-gray-900">
    <div class="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
        <div class="mt-4 md:mt-0">
            <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Our team </h2>
            <p class="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400">  Our team of experienced writers and editors are passionate about sharing knowledge and helping people learn.   We believe that education is a lifelong pursuit and that learning should be accessible to everyone, regardless of their background or education level.
            </p>

        </div>
        <img class="w-full " src="{{asset('images/team.png')}}" alt="team image">

    </div>
</section>

<section class="bg-white dark:bg-gray-900">
    <div class="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
        <img class="w-full " src="{{asset('images/covering.png')}}" alt="dashboard image">
        <div class="mt-4 md:mt-0">
            <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">What We Cover</h2>
            <p class="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400">    At Explanation, we cover a diverse range of topics, including science, technology, history, philosophy, and more. We aim to provide accurate and up-to-date information, and we take great care to ensure that our content is well-researched and reliable.
            </p>

        </div>
    </div>
</section>


    <section class="bg-white dark:bg-gray-900">
        <div class="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
            <div class="mt-4 md:mt-0">
                <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">We encourage feedback </h2>
                <p class="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400"> We also believe in the importance of engaging with our audience and fostering a community of learners. We encourage feedback and discussion and welcome comments and questions on our blog posts.
                </p>

            </div>
            <img class="w-full " src="{{asset('images/feedback.png')}}" alt="team image">

        </div>
    </section>

    <section class="bg-white dark:bg-gray-900">
        <div class="gap-8 items-center py-8 px-4 mx-auto max-w-screen-xl xl:gap-16 md:grid md:grid-cols-2 sm:py-16 lg:px-6">
            <img class="w-full " src="{{asset('images/visit.png')}}" alt="visit image">
            <div class="mt-4 md:mt-0">
                <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">Thank you for visiting</h2>
                <p class="mb-6 font-light text-gray-500 md:text-lg dark:text-gray-400"> We hope you find our content informative and engaging, and we look forward to continuing to share knowledge with our readers.
                </p>

            </div>
        </div>
    </section>


@endsection
