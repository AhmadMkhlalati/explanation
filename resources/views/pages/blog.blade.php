
@extends('layouts.master',['title'=>$blog->title,'type'=>$blog->category->name,'image'=>$blog->image,'description'=>$blog->meta_description,'keywords'=>$blog->meta_keywords])

@section('content')
<style>
    .ql-hidden{
        display: none;
    }
</style>

 <div class="container bg-white mx-auto pt-5">
     <div class="row">

         <div class="grid gap-8 sm:grid-cols-1 lg:grid-cols-1">

             <main class=" dark:bg-gray-900">
                 <div class="flex justify-between px-4 mx-auto max-w-screen-xl ">
                     <article class="mx-auto w-full max-w-5xl format format-sm sm:format-base lg:format-lg format-blue dark:format-invert">
                         <header class="my-2 not-format">

                             <h1 class="mb-4 font-bold text-3xl  leading-tight text-gray-900 lg:mb-6 lg:text-4xl dark:text-white">{{$blog->title}}</h1>
                             <address class="flex items-center mb-2 not-italic">
                                 <div class="inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white">
                                     <div>
                                         <p class="text-base font-light text-gray-500 dark:text-gray-400"><time  datetime="2022-02-08" title="February 8th, 2022">{{$blog->created_at}}</time></p>
                                     </div>
                                 </div>
                             </address>
                         </header>
                         <section class="bg-white dark:bg-gray-900 container grid grid-cols-1 gap-1  my-5 dark:text-white text-gray-900">

                             <div class="" >{!! $blog->introduction !!}</div>
                             {{-- <div class="my-5 bg-gray-200 border-gray-400 w-full h-56 rounded p-5"> Google Ads</div> --}}
                             <div class="">{!! $blog->body !!}</div>
                             {{-- <div class="my-5 bg-gray-200 border-gray-400 w-full h-56 rounded p-5"> Google Ads</div> --}}

                             <div class="">{!! $blog->conclusion !!}</div>



                             <div href="#" class="block p-6  bg-white rounded-lg   dark:bg-gray-800 dark:border-gray-700 ">
                                 <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Tags
                                 </h5>
                                 <div>
                                     @foreach(explode(',', $blog->meta_keywords) as $word)
                                     <span class="bg-blue-100 text-blue-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded dark:bg-blue-200 dark:text-blue-800">{{$word}}</span>
                                     @endforeach
                                 </div>
                             </div>
                             {{-- <div class="my-5 bg-gray-200 border-gray-400  w-full h-56 rounded p-5"> Google Ads</div> --}}

                         </section>


                     </article>
{{--                     <article>--}}
{{--                         <div class="my-5 mx-auto w-[200px] max-w-6xl bg-gray-200 border-gray-400   h-96 rounded p-5"> Google Ads</div>--}}
{{--                         <div class="my-5 mx-auto w-[200px] max-w-6xl bg-gray-200 border-gray-400   h-96 rounded p-5"> Google Ads</div>--}}
{{--                         <div class="my-5 mx-auto w-[200px] max-w-6xl bg-gray-200 border-gray-400   h-96 rounded p-5"> Google Ads</div>--}}

{{--                     </article>--}}

                 </div></main></div>

            <section class="bg-white dark:bg-gray-900">
             <div class="py-8 px-4 mx-auto max-w-screen-xl lg:py-16 lg:px-6">
                 <div class="mx-auto max-w-screen-sm text-center lg:mb-16 mb-8">
                     <h2 class="mb-4 text-3xl lg:text-4xl tracking-tight text-gray-900 dark:text-white">
                         Related articles
                     </h2>

                 </div>
                 <div class="grid mt-4 gap-6 sm:grid-cols-2 lg:grid-cols-4">
                     @foreach($blogs as $blog)
                         @include('components.blog-card', ['blog'=>$blog])
                     @endforeach

                 </div>
{{--                 <div class="text-center">--}}
{{--                     <button type="submit"--}}
{{--                             class="py-3 mt-8 px-5 text-sm font-medium text-center text-white rounded-lg border cursor-pointer bg-blue-700 border-blue-600 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">--}}
{{--                         load more--}}
{{--                     </button>--}}
{{--                 </div>--}}
             </div>
         </section>


     </div>


     </div>
 <script>
      quills = document.getElementsByClassName('ql-editor');
      for (i=0;i<3;i++){
      quills[i].removeAttribute('contenteditable')
      }
     // quills.forEach(item=>{
     //     console.log(item)
     //
     // })
 </script>
@endsection
