@extends('layouts.master')

@section('content')

<div class="bg-white dark:bg-gray-900 items-center mx-auto p-10 ">
    <div class="container mx-5">
    <h3 class="mb-4 text-4xl tracking-tight font-bold text-gray-900 dark:text-white">Privacy Policy for Explanation Site</h3>

    <p class="my-2">At Explanation, we are committed to protecting your privacy. This Privacy Policy outlines the types of information we collect and how we use and protect that information when you visit our site. By using our site, you agree to the terms of this Privacy Policy.</p>

    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Information We Collect</h3>
<ul>
    <li class="my-2">We may collect personal information that you voluntarily provide to us when you subscribe to our blog, comment on our posts, or contact us. This may include your name, email address, and any other information you choose to provide.</li>

    <li class="my-2">In addition, we may collect non-personal information such as your IP address, browser type, and operating system when you visit our site.</li>
</ul>
    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">How We Use Your Information</h3>
    <ul>
    <li class="my-2">We use your personal information to provide you with our blog content and to communicate with you about your subscription or any inquiries you may have. We may also use your information to improve our site and to analyze usage patterns.</li>

    <li class="my-2">We may use non-personal information for analytical purposes and to improve our site.</li>
    </ul>
    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">How We Protect Your Information</h3>
    <ul>
    <li class="my-2">We take reasonable steps to protect your personal information from unauthorized access, disclosure, alteration, or destruction. We use industry-standard security measures to protect your information.</li>

    <li class="my-2">However, no method of transmission over the Internet or electronic storage is completely secure, and we cannot guarantee the absolute security of your information.</li>
</ul>
    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Third-Party Services</h3>
<ul>
<li class="my-2">We may use third-party services such as Google Analytics to analyze our site's usage and improve our content. These services may use cookies or other technologies to collect information about your use of our site.</li>

<li class="my-2">We may also include links to third-party websites on our site. These websites have their own privacy policies, and we are not responsible for their practices.</li>
</ul>
    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Your Rights</h3>

    <p class="my-2">You have the right to access, modify, or delete your personal information that we have collected. You can do this by contacting us through our contact page.</p>

    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Updates to Privacy Policy</h3>

    <p class="my-2">We may update this Privacy Policy from time to time. We will notify you of any significant changes by posting a notice on our site.</p>

    <h3 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Contact Us</h3>

    <p class="my-2">If you have any questions or concerns about this Privacy Policy or our site, please contact us through our contact page.</p>
</div>
</div>
@endsection
