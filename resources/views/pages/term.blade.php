@extends('layouts.master')

@section('content')
    <div class="bg-white dark:bg-gray-900 items-center mx-auto p-10 ">
        <div class="container mx-5">

    <h2 class="mb-4 text-4xl tracking-tight font-bold text-gray-900 dark:text-white">Terms of Service for Explanation Site</h2>

    <p class="my-2">Welcome to Explanation Site. These Terms of Service govern your use of our site. By using our site, you agree to these Terms of Service. If you do not agree to these Terms of Service, please do not use our site.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Intellectual Property</h2>

    <p class="my-2">All content on our site, including text, graphics, images, videos, and software, is the property of Explanation or its content suppliers and is protected by United States and international copyright laws. You may not use our content without our prior written permission.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">User Conduct</h2>

    <p class="my-2">You agree not to use our site for any unlawful or fraudulent activity or in any way that could damage our reputation or interfere with our site's operation. You also agree not to post or transmit any content that is illegal, defamatory, or obscene.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">User Contributions</h2>

    <p class="my-2">Our site may allow you to contribute comments or other content. By submitting content, you grant us a non-exclusive, perpetual, irrevocable, royalty-free, worldwide license to use, reproduce, modify, publish, translate, distribute, and display the content in any media.</p>

    <p class="my-2">You are responsible for any content you contribute, and you represent and warrant that the content is accurate, not confidential, and does not infringe any intellectual property rights.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Disclaimer of Warranties</h2>

    <p class="my-2">Our site is provided on an "as is" and "as available" basis without any warranties, express or implied. We do not guarantee that our site will be uninterrupted or error-free or that any defects will be corrected.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Limitation of Liability</h2>

    <p class="my-2">We will not be liable for any damages arising out of or in connection with your use of our site, including any direct, indirect, incidental, consequential, or punitive damages.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Indemnification</h2>

    <p class="my-2">You agree to indemnify and hold us harmless from any claims, liabilities, damages, and expenses (including attorneys' fees) arising from your use of our site or your breach of these Terms of Service.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Modification of Terms of Service</h2>

    <p class="my-2">We may modify these Terms of Service at any time without notice. By continuing to use our site after we post any modifications, you agree to the modified Terms of Service.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Governing Law</h2>

    <p class="my-2">These Terms of Service are governed by the laws of the United States and the State of California.</p>

    <h2 class="mb-4 text-2xl tracking-tight font-bold text-gray-900 dark:text-white">Contact Us</h2>

    <p class="my-2">If you have any questions or concerns about these Terms of Service or our site, please contact us through our contact page.</p>
        </div>
    </div>
@endsection
