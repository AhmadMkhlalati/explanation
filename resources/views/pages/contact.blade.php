
@extends('layouts.master')

@section('content')
<section class="bg-white">
    <div class="pt-5">
      <h2 class="mb-4 text-4xl tracking-tight  text-center text-gray-900">Connect with us    </h2>
    <p class="mb-8 lg:mb-16 font-light text-center text-gray-500 sm:text-xl">Don't be shy saying hi. We are very happy to communicate with you. We are available 24/7
    </p>

    </div>
  <div
      class="grid max-w-screen-xl px-4  mx-auto lg:gap-8 xl:gap-0 lg:grid-cols-12"
    >
<div class=" max-w-screen-md lg:col-span-7">

   <form action="{{route('message.create')}}" method="post" class="space-y-8 my-5">
       @csrf
       <div>
           <label for="name" class="block mb-2 text-sm font-medium text-gray-900">name</label>
           <input type="name" name="name" id="name" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="Jon , Murry ..." required>
       </div>
        <div>
            <label for="email" class="block mb-2 text-sm font-medium text-gray-900">Email</label>
            <input type="email" name="email" id="email" class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5" placeholder="name@example.com" required>
        </div>
        <div>
            <label for="subject" class="block mb-2 text-sm font-medium text-gray-900">Subject</label>
            <input type="text" name="subject" id="subject" class="block p-3 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 shadow-sm focus:ring-primary-500 focus:border-primary-500" placeholder="Let us know how we can help you" required>
        </div>
        <div class="sm:col-span-2">
            <label for="message" class="block mb-2 text-sm font-medium text-gray-900">Message</label>
            <textarea id="message" name="message" rows="6"  class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-primary-500 focus:border-primary-500" placeholder="Leave a comment..." required></textarea>
        </div>
        <button type="submit" class="py-3 px-5 w-full text-sm font-medium text-center text-white rounded-lg bg-blue-700 sm:w-fit hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-primary-300">Submit</button>
    </form>
</div>
  <div class="lg:col-span-5">
      {{-- <lottie  :options="lottieOptions" v-on:animCreated="handleAnimation" /> --}}
    <div id="letter"></div>
</div></div>
</section>
<script>
    var animationbg = bodymovin.loadAnimation({

container: document.getElementById('letter'),

path: '{{ asset('lottie/contact-us.json') }}',

renderer: 'svg',

loop: true,

autoplay: true,

name: "letter Animation",

});
</script>
@endsection
