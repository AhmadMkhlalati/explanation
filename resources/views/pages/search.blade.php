@extends('layouts.master')

@section('content')
    <section aria-label="Related articles" class="py-4 lg:py-24 bg-gray-50 dark:bg-gray-800">
        <div class="px-4 my-4 mx-auto max-w-screen-xl text-center">
            <h2 class="mb-4 text-3xl lg:text-4xl text-gray-900 dark:text-white">
                Best Result For You Search Query
            </h2>
            <p class="font-light text-gray-500 sm:text-xl dark:text-gray-400">
                These are the articles that you search for : <span style="background-color: #2563eb;
    padding: 5px;
    color: white;
    border-radius: 5px;">{{$query}}</span>.
            </p>
            <div class=" justify-center mx-auto my-5 md:mx-16 sm:mx-0">
                <form method="get" action="{{route('search')}}">
                    @csrf
                    <label for="search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                            <svg aria-hidden="true" class="w-5 h-5 text-gray-500 dark:text-gray-400" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                        </div>
                        <input type="search" id="search" value="{{$query}}" name="query" class="block w-full p-4 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search" required>
                        <button type="submit" class="text-white absolute right-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                    </div>
                </form>
            </div>
            @if(!$blogs )
                <p class="font-light w-full my-12 text-gray-500 sm:text-xl dark:text-gray-400">
                    These is no result  try some thing else
                </p>
            @else

            <div class="grid mt-4 gap-6 sm:grid-cols-2 lg:grid-cols-4">

                @foreach($blogs as $blog)
                    @include('components.blog-card', ['blog'=>$blog])
                @endforeach
            </div>
            @endif

            {{$blogs->withQueryString()->links()}}

        </div>
    </section>

@endsection
