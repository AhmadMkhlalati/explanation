<?php

namespace App\Http\Controllers;

use App\Models\Subscribe;
use Illuminate\Http\Request;

class SubscribeController extends Controller
{
    public function subscribe(Request $request){
        $request->validate(['email'=>'required']);
        Subscribe::create(['email'=>$request->email]);
        return redirect()->back();
    }
}
