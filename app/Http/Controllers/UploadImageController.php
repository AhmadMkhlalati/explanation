<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Storage;

class UploadImageController extends Controller
{
    public static function upload(Request $request, $folderPath = '/images')
    {

            $file = $request->file('file_upload');
            $fileName = uniqid();
            $path = $folderPath;

            if (is_string($file)) {
                $file = substr($file, strpos($file, ",") + 1);
                $file = base64_decode($file);
                $fileName =  $fileName . '.webp';
                $path =  $folderPath . '/' . $fileName;
                // when using base64 it will return true when saving the image
                 Storage::disk('public')->put($path, $file);
                 return ['default'=>$path] ;

            }/* else {
                    $fileName = $file->getClientOriginalName();
                }*/

            // when using image file and upload file it will return the path of the file
            return ['default'=>'/storage/'. (Storage::disk('public')->put($path, $file))];

        }
}
