<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        $data['new']  = Blog::latest('updated_at')->where('mark_id',1)->limit(4)->get();
        $data['feature']  = Blog::latest('updated_at')->where('mark_id',2)->limit(4)->get();
        return view('pages.welcome',compact('data'));
    }
    public function category(){
        $categories =  Category::all();
        return view('pages.categories',compact('categories'));

    }
//    public function CategoryWithBlog($slug){
//        $category = Category::where('slug',$slug)->first();
//        return Blog::where('category_id',$category->id)->paginate(10);
//    }
    public function CategoryBySlug($slug){
        $category = Category::where('slug',$slug)->first();
        $blogs =  Blog::where('category_id',$category->id)->paginate(10);
        return view('pages.categories',compact('blogs'));

    }
    public function BlogBySlug($slug){
        $blog =  Blog::with(['category','mark'])->where('slug',$slug)->first();
        Blog::where('slug',$slug)->update(['visits'=>$blog->visits+1]);//incress count for visiting
        $blogs =  Blog::where('category_id',$blog->category_id)->limit(4)->get();
        return view('pages.blog',compact('blog','blogs'));
    }
}
