<?php

namespace App\Http\Controllers;
use App\Models\Blog;
use App\Models\User;
use App\Models\Mark;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object
     */
    public function getBySlug($slug){
        return Blog::with(['category','mark'])->where('slug',$slug)->first();
    }
    public function index()
    {
        $blogs = Blog::latest('updated_at')->paginate(1);
        return view('admin.blog.index', compact('blogs'));
    }
    public function search(Request $request)
    {
        $request->validate(['query'=>'required']);
        $query = $request['query'];

        $blogs = Blog::latest('updated_at')
            ->where('title','like','%'.$query.'%')
            ->orwhere('introduction','like','%'.$query.'%')
            ->orwhere('body','like','%'.$query.'%')
            ->orwhere('conclusion','like','%'.$query.'%')
            ->orwhere('meta_keywords','like','%'.$query.'%')
            ->orwhere('meta_description','like','%'.$query.'%')
            ->orwhere('introduction','like','%'.$query.'%')
            ->paginate(12);
        return view('pages.search', compact('blogs','query'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
         $marks =  Mark::all();
            // $marks_array = [];
            // foreach ($marks as $mark){
            //     $mars_array[$mark->id] = $mark->flag;
            // }

            $categories =  Category::all();
            // $categories_array = [];
            // foreach ($categories as $category){
            //     $categories_array[$category->id] = $category->name;
            // }

        return view('admin.blog.create',compact('marks','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        Blog::create($data);

        return back()->withSuccess(trans('app.success_store'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Blog::findOrFail($id);

        return view('admin.blogs.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = Blog::findOrFail($id);

        $data = $request->all();

        $item->update($data);

        return redirect()->route(ADMIN . '.blogs.index')->withSuccess(trans('app.success_update'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::destroy($id);

        return back()->withSuccess(trans('app.success_destroy'));
    }

}
