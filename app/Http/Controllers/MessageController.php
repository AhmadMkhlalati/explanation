<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function create(Request $request){
        $request->validate(['name'=>'required','subject'=>'required','email'=>'required','message'=>'required']);
        Message::create([
            'name'=>$request->name,'subject'=>$request->subject,'email'=>$request->email,'message'=>$request->message
        ]);
        return redirect()->back();
    }
}
