<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug','title','introduction','body','conclusion','visits','mark_id','image',
        'meta_keywords',
        'meta_description',
        'viewport',
        'category_id'

    ];

    /*
    |------------------------------------------------------------------------------------
    | Validations
    |------------------------------------------------------------------------------------
    */
    public static function rules($update = false, $id=null)
    {
        return [
            'name' => 'required',
        ];
    }

    /*
    |------------------------------------------------------------------------------------
    | Relations
    |------------------------------------------------------------------------------------
    */

    public function mark(){
        return $this->belongsTo(Mark::class,'mark_id','id');
    }
    public function category(){
        return $this->hasone(Category::class,'id','category_id');
    }
    /*
    |------------------------------------------------------------------------------------
    | Scopes
    |------------------------------------------------------------------------------------
    */

    /*
    |------------------------------------------------------------------------------------
    | Attributes
    |------------------------------------------------------------------------------------
    */

    public function getImageAttribute($value)
    {
        if (!$value) {
            return URL::to('/storage/images') .'/'.'notfound.png';
        }

        return URL::to('/storage/images') .'/'.$value;
    }
    public function setImageAttribute($photo)
    {
        $full_name       = Str::random(16) . '.' . $photo->getClientOriginalExtension();
        $photo->storeAs('public/images/', $full_name);
        $this->attributes['image'] = $full_name ;
    }

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = Str::slug($value);
    }
}
